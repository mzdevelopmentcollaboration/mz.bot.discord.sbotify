using Xunit;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Mz.Bot.Discord.Sbotify.Tests
{
    public class EncryptionTests
    {
        private const string SymmetricKey = "";
        private readonly byte[] _salt = { };

        [Fact]
        public void ConvertByteArrayToString()
        {
            var debug = Encoding.UTF8.GetString(_salt);
        }

        [Fact]
        public void ConvertStringToByteArray()
        {
            var debug = Encoding.UTF8.GetBytes("");
        }

        [Fact]
        public async Task<string> EncryptStringAsync()
        {
            var clearTextBytes = Encoding.Unicode.GetBytes("");
            using var encryptionHandler = Aes.Create();
            var encryptionHelper = new Rfc2898DeriveBytes(SymmetricKey, _salt);
            encryptionHandler.Key = encryptionHelper.GetBytes(32);
            encryptionHandler.IV = encryptionHelper.GetBytes(16);
            await using var memoryStream = new MemoryStream();
            await using var cryptoStream = new CryptoStream(memoryStream,
                encryptionHandler.CreateEncryptor(), CryptoStreamMode.Write);
            cryptoStream.Write(clearTextBytes, 0, clearTextBytes.Length);
            cryptoStream.Close();
            return Convert.ToBase64String(memoryStream.ToArray());
        }

        [Fact]
        public async Task<string> DecryptStringAsync()
        {
            var encryptedTextBytes = Convert.FromBase64String("");
            using var encryptionHandler = Aes.Create();
            var encryptionHelper = new Rfc2898DeriveBytes(SymmetricKey, _salt);
            encryptionHandler.Key = encryptionHelper.GetBytes(32);
            encryptionHandler.IV = encryptionHelper.GetBytes(16);
            await using var memoryStream = new MemoryStream();
            await using var cryptoStream = new CryptoStream(memoryStream,
                encryptionHandler.CreateDecryptor(), CryptoStreamMode.Write);
            cryptoStream.Write(encryptedTextBytes, 0, encryptedTextBytes.Length);
            cryptoStream.Close();
            return Encoding.Unicode.GetString(memoryStream.ToArray());
        }
    }
}
