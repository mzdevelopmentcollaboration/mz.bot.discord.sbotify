﻿using System.Collections.Generic;

namespace Mz.Bot.Discord.Sbotify.Models
{
    internal class SlashCommand
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<SlashCommandOption> Options { get; set; }

        public class SlashCommandOption
        {
            public string Name { get; set; }
            public string Description { get; set; }
            //TODO: Switch OptionType to Enum
            public string OptionType { get; set; }
        }
    }
}
