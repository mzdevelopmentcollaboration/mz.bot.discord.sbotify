﻿using System;
using MongoDB.Bson;
using Newtonsoft.Json;

namespace Mz.Bot.Discord.Sbotify.Models
{
    internal class SpotifyAuthenticationInformation
    {
        public SpotifyAuthenticationInformation()
        {
            Id = ObjectId.GenerateNewId();
            UtcTimeStamp = DateTime.UtcNow;
        }

        public ObjectId Id { get; set; }
        public ulong GuildId { get; set; }
        public DateTime UtcTimeStamp { get; set; }
        [JsonProperty("expires_in")] public int ExpiresIn { get; set; }
        [JsonProperty("token_type")] public string TokenType { get; set; }
        [JsonProperty("access_token")] public string AccessToken { get; set; }
    }
}
