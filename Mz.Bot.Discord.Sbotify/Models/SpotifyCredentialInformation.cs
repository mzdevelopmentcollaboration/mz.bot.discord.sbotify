﻿using System;

using MongoDB.Bson;

namespace Mz.Bot.Discord.Sbotify.Models
{
    internal class SpotifyCredentialInformation
    {
        public SpotifyCredentialInformation()
        {
            Id = ObjectId.GenerateNewId();
            UtcTimeStamp = DateTime.UtcNow;
        }

        public ObjectId Id { get; set; }
        public ulong GuildId { get; set; }
        public string ApplicationId { get; set; }
        public DateTime UtcTimeStamp { get; set; }
        public string EncryptedClientSecret { get; set; }
    }
}
