﻿using System;

using MongoDB.Bson;

namespace Mz.Bot.Discord.Sbotify.Models
{
    internal class GuildInformation
    {
        public GuildInformation(ulong guildId)
        {
            GuildId = guildId;
            Id = ObjectId.GenerateNewId();
            UtcTimeStamp = DateTime.UtcNow;
        }

        public ObjectId Id { get; set; }
        public ulong GuildId { get; set; }
        public DateTime UtcTimeStamp { get; set; }
    }
}
