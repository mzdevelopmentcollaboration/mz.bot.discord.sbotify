﻿namespace Mz.Bot.Discord.Sbotify.Models
{
    internal class DatabaseInformation
    {
        public string Name { get; set; }
        public CollectionInformation CollectionNames { get; set; }

        public class CollectionInformation
        {
            public string Authentication { get; set; }
            public string Credentials { get; set; }
            public string Alerts { get; set; }
            public string Playlists { get; set; }
            public string Guilds { get; set; }
        }
    }
}
