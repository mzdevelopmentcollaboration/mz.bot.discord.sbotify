﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;

using Mz.Bot.Discord.Sbotify.Models;
using Mz.Bot.Discord.Sbotify.Managers;

// ReSharper disable ForeachCanBePartlyConvertedToQueryUsingAnotherGetEnumerator

namespace Mz.Bot.Discord.Sbotify.Worker
{
    internal class TokenWorker : BackgroundService
    {
        private readonly LogManager _logManager;
        private readonly SpotifyManager _spotifyManager;
        private readonly DatabaseManager _databaseManager;
        private readonly CryptographyManager _cryptographyManager;

        public TokenWorker(IConfiguration configuration)
        {
            _logManager = new LogManager();
            _spotifyManager = new SpotifyManager();
            _databaseManager = new DatabaseManager(
                Environment.GetEnvironmentVariable("DatabaseConnectionString"),
                configuration.GetSection("DatabaseSettings").Get<DatabaseInformation>());
            _cryptographyManager = new CryptographyManager(Environment.GetEnvironmentVariable("SymmetricKey"),
                Environment.GetEnvironmentVariable("Salt"));
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            foreach (var spotifyAuthenticationInformation in await _databaseManager
                .FetchSpotifyAuthenticationInformationAsync())
            {
                if ((DateTime.UtcNow - spotifyAuthenticationInformation.UtcTimeStamp).Minutes <= 1) continue;
                _logManager.LogContent(
                    $"AuthenticationInformation for [{spotifyAuthenticationInformation.GuildId}] will be replaced");
                var spotifyCredentialInformation = await _databaseManager
                    .FetchSpotifyCredentialInformation(spotifyAuthenticationInformation.GuildId);
                await _databaseManager.UpdateSpotifyAuthenticationInformationAsync(
                    await _spotifyManager
                        .FetchSpotifyApiAuthenticationInformationAsync(spotifyCredentialInformation.ApplicationId,
                            await _cryptographyManager.DecryptStringAsync(
                                spotifyCredentialInformation.EncryptedClientSecret)),
                    spotifyAuthenticationInformation.Id);
            }
            await Task.Delay(15000, stoppingToken);
        }
    }
}
