using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;

using Discord;
using Mz.Bot.Discord.Sbotify.Models;
using Mz.Bot.Discord.Sbotify.Managers;

using static Mz.Bot.Discord.Sbotify.Program;

namespace Mz.Bot.Discord.Sbotify.Worker
{
    public class BotWorker : BackgroundService
    {
        private readonly DiscordManager _discordManager;

        public BotWorker(IConfiguration configuration)
        {
            var databaseConnectionString = 
                Environment.GetEnvironmentVariable("DatabaseConnectionString");
            var databaseInformation =
                configuration.GetSection("DatabaseSettings").Get<DatabaseInformation>();
            _discordManager = new DiscordManager(DiscordClient,
                configuration.GetSection("SlashCommands").Get<SlashCommand[]>(),
                databaseInformation, databaseConnectionString,
                    Environment.GetEnvironmentVariable("SymmetricKey"),
                    Environment.GetEnvironmentVariable("Salt"));
        }

        public override async Task StartAsync(CancellationToken cancellationToken)
        {
            DiscordClient.Log += _discordManager.DiscordClientOnLog;
            DiscordClient.Ready += _discordManager.DiscordClientOnReady;
            DiscordClient.JoinedGuild += _discordManager.DiscordClientOnJoinedGuild;
            DiscordClient.MessageReceived += _discordManager.DiscordClientOnMessageReceived;
            DiscordClient.SlashCommandExecuted +=
                _discordManager.DiscordClientOnSlashCommandExecuted;
            //TODO: Do not provide the Token in Code
            await DiscordClient.LoginAsync(TokenType.Bot,
                "");
            await DiscordClient.StartAsync();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(-1, stoppingToken);
            }
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            await DiscordClient.StopAsync();
            await DiscordClient.LogoutAsync();
        }
    }
}