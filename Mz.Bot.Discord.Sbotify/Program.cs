using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;

using Serilog;
using Discord.WebSocket;
using Serilog.Sinks.SystemConsole.Themes;

using Mz.Bot.Discord.Sbotify.Worker;

namespace Mz.Bot.Discord.Sbotify
{
    public class Program
    {
        public static readonly DiscordSocketClient DiscordClient = new();

        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console(theme: AnsiConsoleTheme.Code,
                    outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj} {NewLine}")
                .CreateLogger();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureLogging(config =>
                {
                    config.ClearProviders();
                })
                .ConfigureServices(services =>
                {
                    services.AddHostedService<BotWorker>();
                    services.AddHostedService<TokenWorker>();
                });
    }
}