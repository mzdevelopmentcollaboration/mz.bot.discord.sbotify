﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;

using Mz.Bot.Discord.Sbotify.Models;

namespace Mz.Bot.Discord.Sbotify.Managers
{
    internal class DiscordCommandManager
    {
        private readonly LogManager _logManager = new();
        private readonly DatabaseManager _databaseManager;
        private readonly CryptographyManager _cryptographyManager;

        public DiscordCommandManager(DatabaseManager databaseManager,
            string symmetricKey, string salt)
        {
            _databaseManager = databaseManager;
            _cryptographyManager = new CryptographyManager(symmetricKey, salt);
        }

        public async Task<string> RegisterSpotifyApiCredentialsAsync(SocketMessage message)
        {
            string[] spotifyApiCredentials;
            try
            {
                if (!message.Content.Contains(';'))
                {
                    throw new ArgumentException(
                        "Supplied credential seems to be incorrectly formatted! Please try again using different values!");
                }
                spotifyApiCredentials = message.Content.Split(';');
                if (spotifyApiCredentials.Length != 3)
                {
                    throw new ArgumentException(
                        "Supplied credential seems to be incorrectly formatted! Please try again using different values!");
                }
                if (!ulong.TryParse(spotifyApiCredentials[2], out _))
                {
                    throw new ArgumentException(
                        "No GuildId seems to be supplied! Please try again using different values!");
                }
            }
            catch (Exception genericException)
            {
                return genericException.Message;
            }
            var parsedGuildId = ulong.Parse(spotifyApiCredentials[2]);
            if (!await _databaseManager.CheckForGuildAsync(parsedGuildId))
            {
                return "Bot has not yet joined the supplied Guild! Please let the bot join your guild first!";
            }
            SpotifyAuthenticationInformation spotifyApiAuthenticationInformation;
            try
            {
                spotifyApiAuthenticationInformation = await new SpotifyManager().FetchSpotifyApiAuthenticationInformationAsync(
                    spotifyApiCredentials[0], spotifyApiCredentials[1]);
            }
            catch (Exception genericException)
            {
                _logManager.LogContent(genericException.Message, LogSeverity.Error);
                return "Supplied credential seem to be invalid! Please try again using different values!";
            }
            spotifyApiAuthenticationInformation.GuildId = parsedGuildId;
            await _databaseManager.InsertSpotifyAuthenticationInformationAsync(spotifyApiAuthenticationInformation);
            await _databaseManager.InsertSpotifyCredentialInformationAsync(
                new SpotifyCredentialInformation
                {
                    ApplicationId = spotifyApiCredentials[0],
                    EncryptedClientSecret =
                        await _cryptographyManager.EncryptStringAsync(spotifyApiCredentials[1]),
                    GuildId = parsedGuildId
                });
            return "Credential has been successfully registered!";
        }
    }
}
