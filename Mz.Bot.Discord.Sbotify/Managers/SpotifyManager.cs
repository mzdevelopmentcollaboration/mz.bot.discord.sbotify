﻿using System;
using System.IO;
using System.Net;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Newtonsoft.Json;

using Mz.Bot.Discord.Sbotify.Models;

namespace Mz.Bot.Discord.Sbotify.Managers
{
    internal class SpotifyManager
    {
        public async Task<SpotifyAuthenticationInformation> FetchSpotifyApiAuthenticationInformationAsync(
            string spotifyApiClientId, string spotifyApiClientSecret)
        {
            var authorizationStringBytes = Encoding.UTF8.GetBytes($"{spotifyApiClientId}:{spotifyApiClientSecret}");
            var webRequestParameter = Encoding.UTF8.GetBytes("grant_type=client_credentials");
            var webRequest = WebRequest.Create("https://accounts.spotify.com/api/token");
            webRequest.Method = "POST";
            webRequest.Headers.Add("Authorization", $"Basic {Convert.ToBase64String(authorizationStringBytes)}");
            webRequest.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            var webRequestStream = await webRequest.GetRequestStreamAsync();
            await webRequestStream.WriteAsync(webRequestParameter, 0, webRequestParameter.Length);
            webRequestStream.Close();
            try
            {
                var webRequestResponse = await webRequest.GetResponseAsync();
                var webRequestResponseStream = webRequestResponse.GetResponseStream();
                using var webRequestResponseStreamReader = new StreamReader(webRequestResponseStream ??
                    throw new InvalidOperationException("Response-Stream during Spotify-API-Interaction was Null!"));
                return JsonConvert.DeserializeObject<SpotifyAuthenticationInformation>(
                    await webRequestResponseStreamReader.ReadToEndAsync());
            }
            catch (Exception genericException)
            {
                new LogManager().LogContent(genericException.Message, LogSeverity.Error);
                throw new AuthenticationException("Invalid spotify-api-credentials were provided!");
            }
        }
    }
}
