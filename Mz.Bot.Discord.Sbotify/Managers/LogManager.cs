﻿using Discord;
using Serilog;

namespace Mz.Bot.Discord.Sbotify.Managers
{
    internal class LogManager
    {
        private readonly ILogger _logger;

        public LogManager()
        {
            _logger = Log.Logger;
        }

        public void LogContent(string content) =>
            _logger.Information(content);


        public void LogContent(string content, LogSeverity severity) =>
            _logger.Information($"[{severity}] {content}");
    }
}