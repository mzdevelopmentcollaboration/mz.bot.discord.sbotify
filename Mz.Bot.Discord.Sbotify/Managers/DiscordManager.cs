﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Discord;
using Discord.Net;
using Newtonsoft.Json;
using Discord.WebSocket;

using Mz.Bot.Discord.Sbotify.Models;

namespace Mz.Bot.Discord.Sbotify.Managers
{
    internal class DiscordManager
    {
        private readonly LogManager _logger;
        private readonly SlashCommand[] _slashCommands;
        private readonly DatabaseManager _databaseManager;
        private readonly DiscordSocketClient _discordClient;
        private readonly DiscordEmbedManager _discordEmbedManager;
        private readonly DiscordCommandManager _discordCommandManager;

        public DiscordManager(DiscordSocketClient discordClient,
            SlashCommand[] slashCommands, DatabaseInformation databaseInformation,
            string databaseConnectionString, string symmetricKey, string salt)
        {
            _logger = new LogManager();
            _discordClient = discordClient;
            _slashCommands = slashCommands;
            _discordEmbedManager = new DiscordEmbedManager();
            _databaseManager = new DatabaseManager(databaseConnectionString, databaseInformation);
            _discordCommandManager =
                new DiscordCommandManager(_databaseManager, symmetricKey, salt);
        }

        public Task DiscordClientOnLog(LogMessage logMessage)
        {
            _logger.LogContent(logMessage.Message, logMessage.Severity);
            return Task.CompletedTask;
        }

        public async Task DiscordClientOnReady()
        {
            //TODO: Do not provide the GuildId in Code, switch to
            //client.CreateGlobalApplicationCommandAsync(globalCommand.Build());
            var guild = _discordClient.GetGuild(1);
            try
            {
                foreach (var slashCommand in BuildSlashCommands())
                {
                    //await guild.CreateApplicationCommandAsync(slashCommand);
                }
            }
            catch (ApplicationCommandException exception)
            {
                _logger.LogContent(
                    JsonConvert.SerializeObject(exception.Error, Formatting.Indented));
            }
        }

        public async Task DiscordClientOnJoinedGuild(SocketGuild guild)
        {
            await _databaseManager.InsertGuildInformationAsync(new GuildInformation(guild.Id));
        }

        public async Task DiscordClientOnMessageReceived(SocketMessage message)
        {
            if (message.Author.IsBot) return;
            await message.Channel.SendMessageAsync(
                embed: _discordEmbedManager.GenerateDefaultResponse(
                    await _discordCommandManager.RegisterSpotifyApiCredentialsAsync(message)));
        }

        public async Task DiscordClientOnSlashCommandExecuted(SocketSlashCommand command)
        {
            switch (command.Data.Name)
            {
                case "splaylist":
                    switch (command.Data.Options.First().Name)
                    {
                        case "add":
                            /*
                            await _discordCommandManager.RegisterSpotifyApiCredentialsAsync(command,
                                command.Data
                                    .Options.First().Value.ToString());
                            */
                            break;
                    }
                    break;
                case "sregister":
                    //TODO: Create registration entry within the database
                    await command.RespondAsync("You will receive further registration steps in a private chat!");
                    var dmChannel = await command.User.CreateDMChannelAsync();
                    await dmChannel.SendMessageAsync("Please enter your application credentials:");
                    break;
            }
        }

        private IEnumerable<SlashCommandProperties> BuildSlashCommands()
        {
            if (_slashCommands == null)
            {
                throw new NullReferenceException(
                    "SlashCommands are missing from configuration-file");
            }
            var slashCommandCollection = new List<SlashCommandProperties>();
            // ReSharper disable once PossibleNullReferenceException
            foreach (var slashCommand in _slashCommands)
            {
                var slashCommandBuilder = new SlashCommandBuilder();
                slashCommandBuilder.WithName(slashCommand.Name);
                slashCommandBuilder.WithDescription(slashCommand.Description);
                if (slashCommand.Options != null)
                {
                    foreach (var slashCommandOption in slashCommand.Options)
                    {
                        var slashCommandOptionType = slashCommandOption.OptionType switch
                        {
                            "String" => ApplicationCommandOptionType.String,
                            "Integer" => ApplicationCommandOptionType.Integer,
                            _ => ApplicationCommandOptionType.String
                        };
                        slashCommandBuilder.AddOption(new SlashCommandOptionBuilder()
                            .WithName(slashCommandOption.Name)
                            .WithDescription(slashCommandOption.Description)
                            .WithType(slashCommandOptionType));
                    }
                }
                slashCommandCollection.Add(slashCommandBuilder.Build());
            }
            return slashCommandCollection;
        }
    }
}