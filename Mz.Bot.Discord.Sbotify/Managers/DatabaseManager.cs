﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Discord;
using MongoDB.Bson;
using MongoDB.Driver;

using Mz.Bot.Discord.Sbotify.Models;

namespace Mz.Bot.Discord.Sbotify.Managers
{
    internal class DatabaseManager
    {
        private readonly LogManager _logManager;
        private readonly IMongoCollection<SpotifyAuthenticationInformation>
            _spotifyAuthenticationInformationCollection;
        private readonly IMongoCollection<SpotifyCredentialInformation>
            _spotifyCredentialInformationCollection;
        private readonly IMongoCollection<GuildInformation>
            _spotifyGuildInformationCollection;

        public DatabaseManager(string databaseConnectionString,
            DatabaseInformation databaseInformation)
        {
            _logManager = new LogManager();
            var databaseClient = new MongoClient(databaseConnectionString);
            var database = databaseClient.GetDatabase(databaseInformation.Name);
            _spotifyGuildInformationCollection =
                database.GetCollection<GuildInformation>(databaseInformation.CollectionNames.Guilds);
            _spotifyAuthenticationInformationCollection =
                database.GetCollection<SpotifyAuthenticationInformation>(databaseInformation
                    .CollectionNames.Authentication);
            _spotifyCredentialInformationCollection =
                database.GetCollection<SpotifyCredentialInformation>(databaseInformation
                    .CollectionNames.Credentials);
        }

        public async Task InsertSpotifyCredentialInformationAsync(
            SpotifyCredentialInformation spotifyCredentialInformation) =>
            await _spotifyCredentialInformationCollection.InsertOneAsync(
                spotifyCredentialInformation);
        

        public async Task InsertSpotifyAuthenticationInformationAsync(
            SpotifyAuthenticationInformation spotifyAuthenticationInformation) =>
            await _spotifyAuthenticationInformationCollection.InsertOneAsync(
                spotifyAuthenticationInformation);

        public async Task<List<SpotifyAuthenticationInformation>> FetchSpotifyAuthenticationInformationAsync()
        {
            var collectionCursor =
                await _spotifyAuthenticationInformationCollection.FindAsync(
                    FilterDefinition<SpotifyAuthenticationInformation>.Empty);
            return await collectionCursor.ToListAsync();
        }

        //TODO: Create generic method for insertion
        public async Task InsertGuildInformationAsync(GuildInformation guildInformation) =>
            await _spotifyGuildInformationCollection.InsertOneAsync(guildInformation);

        public async Task<bool> CheckForGuildAsync(ulong guildId)
        {
            var guildsCursor = await _spotifyGuildInformationCollection.FindAsync(x => x.GuildId == guildId);
            var guilds = await guildsCursor.ToListAsync();
            return guilds.Any();
        }

        public async Task<SpotifyCredentialInformation> FetchSpotifyCredentialInformation(ulong guildId)
        {
            var credentialCursor = await _spotifyCredentialInformationCollection.FindAsync(
                x => x.GuildId == guildId);
            var credentials = await credentialCursor.ToListAsync();
            return credentials.FirstOrDefault();
        }

        public async Task UpdateSpotifyAuthenticationInformationAsync(
            SpotifyAuthenticationInformation spotifyAuthenticationInformation, ObjectId objectId)
        {
            var updateDefinition =
                Builders<SpotifyAuthenticationInformation>.Update
                    .Set("UtcTimeStamp", DateTime.UtcNow)
                    .Set("AccessToken", spotifyAuthenticationInformation.AccessToken);
            try
            {
                await _spotifyAuthenticationInformationCollection.FindOneAndUpdateAsync(
                    x => x.Id == objectId, updateDefinition);
            }
            catch (Exception genericException)
            {
                _logManager.LogContent(genericException.Message, LogSeverity.Error);
            }
        }
    }
}
