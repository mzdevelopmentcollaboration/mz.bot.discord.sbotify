﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;

namespace Mz.Bot.Discord.Sbotify.Managers
{
    internal class DiscordEmbedManager
    {
        public Embed GenerateDefaultResponse(string description)
        {
            var embedBuilder = new EmbedBuilder();
            embedBuilder.WithTitle("Sbotify")
                .WithColor(Color.Green)
                .WithDescription(description);
            return embedBuilder.Build();
        }
    }
}
