﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Mz.Bot.Discord.Sbotify.Managers
{
    internal class CryptographyManager
    {
        private readonly byte[] _salt;
        private readonly string _symmetricKey;

        public CryptographyManager(string symmetricKey, string salt)
        {
            _symmetricKey = symmetricKey;
            _salt = Encoding.UTF8.GetBytes(salt);
        }
        
        public async Task<string> EncryptStringAsync(string clearText)
        {
            var clearTextBytes = Encoding.Unicode.GetBytes(clearText);
            using var encryptionHandler = Aes.Create();
            var encryptionHelper = new Rfc2898DeriveBytes(_symmetricKey, _salt);
            encryptionHandler.Key = encryptionHelper.GetBytes(32);
            encryptionHandler.IV = encryptionHelper.GetBytes(16);
            await using var memoryStream = new MemoryStream();
            await using var cryptoStream = new CryptoStream(memoryStream,
                encryptionHandler.CreateEncryptor(), CryptoStreamMode.Write);
            cryptoStream.Write(clearTextBytes, 0, clearTextBytes.Length);
            cryptoStream.Close();
            return Convert.ToBase64String(memoryStream.ToArray());
        }
        
        public async Task<string> DecryptStringAsync(string encryptedText)
        {
            var encryptedTextBytes = Convert.FromBase64String(encryptedText);
            using var encryptionHandler = Aes.Create();
            var encryptionHelper = new Rfc2898DeriveBytes(_symmetricKey, _salt);
            encryptionHandler.Key = encryptionHelper.GetBytes(32);
            encryptionHandler.IV = encryptionHelper.GetBytes(16);
            await using var memoryStream = new MemoryStream();
            await using var cryptoStream = new CryptoStream(memoryStream,
                encryptionHandler.CreateDecryptor(), CryptoStreamMode.Write);
            cryptoStream.Write(encryptedTextBytes, 0, encryptedTextBytes.Length);
            cryptoStream.Close();
            return Encoding.Unicode.GetString(memoryStream.ToArray());
        }
    }
}
